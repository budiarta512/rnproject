import {
  CompositeScreenProps,
  NavigationContainer,
  NavigatorScreenParams,
} from '@react-navigation/native';
import {
  NativeStackNavigationProp,
  NativeStackScreenProps,
  createNativeStackNavigator,
} from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react';
import Home from '../screens/Home';
import Login from '../screens/auth/Login';
import Register from '../screens/auth/Register';
import {
  BottomTabScreenProps,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import {checkAuth} from '../services/authStorage';
import Transaction from '../screens/transaction/Transaction';
import Pos from '../screens/pos/Pos';
import Account from '../screens/account/Account';
import {ColorValue} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export type TabStackParams = {
  Home: undefined;
  Pos: undefined;
  Transaction: undefined;
  Account: undefined;
};

export type RootStackParams = {
  Home: NavigatorScreenParams<TabStackParams>;
  Login: undefined;
  Register: undefined;
};

export type HomeScreenNavigationProp =
  NativeStackNavigationProp<RootStackParams>;

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator<TabStackParams>();

export type RootStackScreenProps<T extends keyof RootStackParams> =
  NativeStackScreenProps<RootStackParams, T>;

export type TabStackScreenkProps<T extends keyof TabStackParams> =
  CompositeScreenProps<
    BottomTabScreenProps<TabStackParams, T>,
    RootStackScreenProps<keyof RootStackParams>
  >;

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParams {}
  }
}

// Icons for Bottom Tab Navigation
const homeIcon = ({color}: {color: ColorValue | number}) => (
  <Icon name="home" size={30} color={color} />
);
const PosIcon = ({color}: {color: ColorValue | number}) => (
  <Icon name="cash-register" size={30} color={color} />
);
const transactionIcon = ({color}: {color: ColorValue | number}) => (
  <Icon name="bank-transfer" size={24} color={color} />
);
const AccountIcon = ({color}: {color: ColorValue | number}) => (
  <Icon name="account" size={24} color={color} />
);

const TabHome = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerTitle: () => null,
        tabBarShowLabel: false,
        headerTitleAlign: 'center',
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          title: 'Home',
          tabBarIcon: homeIcon,
        }}
      />
      <Tab.Screen
        name="Pos"
        component={Pos}
        options={{
          title: 'Pos',
          tabBarIcon: PosIcon,
        }}
      />
      <Tab.Screen
        name="Transaction"
        component={Transaction}
        options={{
          title: 'Transaction',
          tabBarIcon: transactionIcon,
        }}
      />
      <Tab.Screen
        name="Account"
        component={Account}
        options={{
          title: 'Account',
          tabBarIcon: AccountIcon,
        }}
      />
    </Tab.Navigator>
  );
};

const RootNavigator = () => {
  const [isLogin, setIsLogin] = useState(false);
  useEffect(() => {
    checkAuth().then(res => {
      if (res !== null) {
        setIsLogin(true);
      } else {
        setIsLogin(false);
      }
    });
  });
  return (
    <NavigationContainer>
      {isLogin ? (
        <Stack.Navigator initialRouteName="TabHome">
          <Stack.Screen name="TabHome" component={TabHome} />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen
            options={{headerShown: false}}
            name="Login"
            component={Login}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="Register"
            component={Register}
          />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};

export default RootNavigator;
