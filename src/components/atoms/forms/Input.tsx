import {
  Text,
  TextInputChangeEventData,
  NativeSyntheticEvent,
  TextInputProps,
  View,
  TextInput,
  StyleSheet,
} from 'react-native';
import React, {useState} from 'react';

type props = {
  label?: JSX.Element | JSX.Element[] | string | string[];
  onChange: (e: NativeSyntheticEvent<TextInputChangeEventData>) => void;
  value: string;
} & TextInputProps;

const Input = ({label, onChange, value, ...rest}: props) => {
  const [focus, setFocus] = useState(false);
  return (
    <View>
      {label && <Text>{label}</Text>}
      <TextInput
        {...rest}
        onChange={onChange}
        value={value}
        style={[styles.base, focus ? styles.inputFocus : styles.input]}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  base: {
    paddingVertical: 2,
    paddingHorizontal: 6,
    borderRadius: 8,
  },
  input: {
    borderWidth: 1.5,
    borderColor: '#888',
  },
  inputFocus: {
    borderWidth: 2.5,
    borderColor: '#81689D',
  },
});
