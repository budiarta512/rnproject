import React, {useEffect, useState} from 'react';
import Card from '../../components/organisms/Card';
import Input from '../../components/atoms/forms/Input';
import Button from '../../components/atoms/buttons/Button';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import ErrorMessage from '../../components/atoms/forms/ErrorMessage';
import {signin} from '../../services/authService';
import {checkAuth, setAuth} from '../../services/authStorage';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  // const [refresh, setRefresh] = useState()

  useEffect(() => {
    checkAuth();
  }, []);

  const handleSignin = async () => {
    setLoading(true);
    try {
      const user = await signin(email, password);
      if (user) {
        console.log('logged in', user.user);
        const auth = user.user;
        const token = await user.user.getIdTokenResult().then(res => res.token);
        await setAuth({
          accessToken: token,
          displayName: auth.displayName,
          email: auth.email,
          emailVerified: auth.emailVerified,
          isAnonymous: auth.isAnonymous,
          phoneNumber: auth.phoneNumber,
          photoURL: auth.photoURL,
          tenantId: auth.tenantId,
          uid: auth.uid,
        });
      }
      setLoading(false);
    } catch (err: any) {
      switch (err.code) {
        case 'auth/invalid-email':
          console.log('format email salah');
          break;
        case 'auth/missing-password':
          console.log('password harus isi');
          break;
        case 'auth/invalid-credential':
          console.log('email atau password salah');
          break;
        default:
          console.log('signup error', err, err.code);
      }
      console.log('err', err);
      setLoading(false);
    }
  };
  return (
    <View style={styles.container}>
      <Card>
        <Text style={styles.title}>Login Akun</Text>
        <View>
          <Input
            label={'Email'}
            value={email}
            onChange={e => setEmail(e.nativeEvent.text)}
          />
          <ErrorMessage message="" />
        </View>
        <View>
          <Input
            label={'Password'}
            value={password}
            secureTextEntry
            onChange={e => setPassword(e.nativeEvent.text)}
          />
          <ErrorMessage message="" />
        </View>
        <View>
          <Button onPress={handleSignin} isLoading={loading}>
            Login
          </Button>
        </View>
        <Text style={styles.registerText}>
          Belum punya akun?{' '}
          <Text
            onPress={() => navigation.navigate('Register')}
            style={styles.registerTextChild}>
            Buat Sekarang
          </Text>
        </Text>
      </Card>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 12,
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
  },
  registerText: {
    color: '#333',
    textAlign: 'center',
  },
  registerTextChild: {
    color: '#40A2E3',
  },
});
