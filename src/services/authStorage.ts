import AsyncStorage from '@react-native-async-storage/async-storage';
import {ILogin} from './authService';

export const checkAuth = async () => {
  const auth = await AsyncStorage.getItem('auth');
  console.log('auth', auth);
  if (auth) {
    return auth;
  }
  return null;
};

export const setAuth = async (loginData: ILogin) => {
  const userData = {
    email: loginData.email,
    phoneNumber: loginData.phoneNumber,
    emailVerified: loginData.emailVerified,
    access_token: loginData.accessToken,
    id: loginData.uid,
  };
  await AsyncStorage.setItem('auth', JSON.stringify(userData));
};
