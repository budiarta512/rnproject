// Types
interface spacingType {
  borderRadius: number;
  layoutPaddingH: number;
  layoutPaddingV: number;
  containerPaddingV: number;
  cardMarginB: number;
  layoutMarginB: number;
}

interface typeSizesType {
  FONT_SIZE_SMALL: number;
  FONT_SIZE_MEDIUM: number;
  FONT_SIZE_LARGE: number;
  FONT_STYLE_ITALIC: 'italic';
  TEXT_STYLE_CAPITALIZE: 'capitalize';
  FONT_WEIGHT_LIGHT:
    | '600'
    | 'bold'
    | 'normal'
    | '100'
    | '200'
    | '300'
    | '400'
    | '500'
    | '700'
    | '800'
    | '900'
    | undefined;
  FONT_WEIGHT_MEDIUM:
    | '600'
    | 'bold'
    | 'normal'
    | '100'
    | '200'
    | '300'
    | '400'
    | '500'
    | '700'
    | '800'
    | '900'
    | undefined;
  FONT_WEIGHT_HEAVY:
    | '600'
    | 'bold'
    | 'normal'
    | '100'
    | '200'
    | '300'
    | '400'
    | '500'
    | '700'
    | '800'
    | '900'
    | undefined;
}

export interface themeType {
  name: string;
  color: string;
  text: string;
  primary: string;
  primaryDark: string;
  secondary: string;
  layoutBg: string;
  cardBg: string;
  cardBorderColor: string;
  error: string;
}

interface themesType {
  light: themeType;
  dark: themeType;
}

// Spacing:- Common margins and paddings
const spacing: spacingType = {
  borderRadius: 16,
  layoutPaddingH: 16,
  layoutPaddingV: 12,
  containerPaddingV: 22,
  cardMarginB: 16,
  layoutMarginB: 8,
};

// Type Sizes:- Font sizes and weights
const typeSizes: typeSizesType = {
  FONT_SIZE_SMALL: 12,
  FONT_SIZE_MEDIUM: 14,
  FONT_SIZE_LARGE: 17,
  FONT_WEIGHT_LIGHT: '200',
  FONT_WEIGHT_MEDIUM: '600',
  FONT_WEIGHT_HEAVY: '800',
  FONT_STYLE_ITALIC: 'italic',
  TEXT_STYLE_CAPITALIZE: 'capitalize',
};

// Themes:- Can alter values here. Can only be consumed through Context (see useTheme.js file)
const themes: themesType = {
  light: {
    name: 'light',
    color: '#695D5D',
    text: '#1e1e1e',
    primary: '#785fee',
    primaryDark: '#2B006D',
    secondary: '#FF0075',
    layoutBg: '#F0FDFE',
    cardBg: '#ffffff',
    cardBorderColor: '#EEECEC',
    error: '#B00020',
  },
  dark: {
    name: 'dark',
    color: '#ffffff',
    text: '#ffffff',
    primary: '#785fee',
    primaryDark: '#2B006D',
    secondary: '#FF0075',
    layoutBg: '#404040',
    cardBg: '#1e1e1e',
    cardBorderColor: '#1A1A1A',
    error: '#B00020',
  },
};

export {spacing, typeSizes, themes};
