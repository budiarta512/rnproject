import {View, Text, StyleSheet} from 'react-native';
import React from 'react';

type props = {
  message?: string;
};
const ErrorMessage = ({message}: props) => {
  if (!message) {
    return null;
  }
  return (
    <View>
      <Text style={styles.errorMessageText}>{message}</Text>
    </View>
  );
};

export default ErrorMessage;

const styles = StyleSheet.create({
  errorMessageText: {
    color: '#FF8080',
  },
});
