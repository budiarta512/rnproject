import {
  ActivityIndicator,
  GestureResponderEvent,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

type props = {
  className?: string;
  onPress: (e: GestureResponderEvent) => void;
  children: string | string[] | JSX.Element | JSX.Element[];
  isLoading?: boolean;
};

const Button = ({onPress, children, isLoading}: props) => {
  const finalChildren =
    typeof children === 'string' ? (
      <Text style={styles.textButton}>{children}</Text>
    ) : (
      children
    );

  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      {isLoading ? <ActivityIndicator /> : finalChildren}
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blue',
    padding: 8,
    borderRadius: 16,
  },
  textButton: {
    color: 'white',
  },
});
