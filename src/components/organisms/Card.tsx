import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

type props = {
  children: JSX.Element | JSX.Element[] | string | string[];
  onPress?: (a: any) => void;
};

const Card = ({children, onPress}: props) => {
  if (onPress) {
    return (
      <TouchableOpacity style={styles.base} onPress={onPress}>
        {children}
      </TouchableOpacity>
    );
  }
  return <View style={styles.base}>{children}</View>;
};

export default Card;

const styles = StyleSheet.create({
  base: {
    backgroundColor: '#FFFFFF',
    gap: 12,
    padding: 12,
    borderRadius: 12,
  },
});
