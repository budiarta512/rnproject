// Import the functions you need from the SDKs you need
import {initializeApp} from 'firebase/app';
import {getAnalytics} from 'firebase/analytics';
import {getFirestore} from 'firebase/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'asdasdasd',
  authDomain: 'asdasdasd.firebaseapp.com',
  projectId: 'asdasdasd',
  storageBucket: 'asdasdasd.appspot.com',
  messagingSenderId: '1231231313131',
  appId: '1:1231231313131:web:b35deaf2039fb0d7afb77e',
  measurementId: 'G-dasdasda',
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);
export const db = getFirestore(app);
