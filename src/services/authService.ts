import {
  createUserWithEmailAndPassword,
  getAuth,
  sendEmailVerification,
  signInWithEmailAndPassword,
} from 'firebase/auth';
import {app, db} from './config';
import {User} from 'firebase/auth/web-extension';
import {Timestamp, addDoc, collection} from 'firebase/firestore';

const auth = getAuth(app);

export const signin = async (email: string, password: string) => {
  try {
    const loginUser = await signInWithEmailAndPassword(auth, email, password);
    console.log('user signed', loginUser);
    return loginUser;
  } catch (err) {
    throw err;
  }
};

export const signup = async (email: string, password: string) => {
  try {
    const userCredential = await createUserWithEmailAndPassword(
      auth,
      email,
      password,
    );
    await emailVerification(userCredential.user);
    const user = userCredential.user;
    console.log('user registered', user);
    return user;
  } catch (err) {
    throw err;
  }
};

export const emailVerification = async (userCredential: User) => {
  const user = auth.currentUser;
  try {
    await sendEmailVerification(user || userCredential, {
      handleCodeInApp: true,
      url: 'http://localhost:3000/login',
    }).then(() => {
      console.log('verification sent');
    });
  } catch (err: any) {
    const errorCode = err.code;
    const errMassage = err.message;
    console.log('Email verification error', errorCode, errMassage);
    throw err;
  }
};

export const saveUserData = async (id: string, firstName: string) => {
  await addDoc(collection(db, 'users'), {
    id,
    firstName,
    created: Timestamp.now(),
  });
};

export interface ILogin {
  displayName: string | undefined | null;
  email: string | null;
  emailVerified: boolean;
  isAnonymous: boolean;
  phoneNumber: string | undefined | null;
  photoURL: string | undefined | null;
  accessToken: string;
  tenantId: string | undefined | null;
  uid: string;
}
