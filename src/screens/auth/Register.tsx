import React, {useState} from 'react';
import Card from '../../components/organisms/Card';
// import {useNavigation} from '@react-navigation/native';
import {saveUserData, signup} from '../../services/authService';
import Input from '../../components/atoms/forms/Input';
import Button from '../../components/atoms/buttons/Button';
import ErrorMessage from '../../components/atoms/forms/ErrorMessage';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const Register = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  //   const navigation = useNavigation();

  const navigation = useNavigation();

  const handleSignup = async () => {
    setLoading(true);
    try {
      const user = await signup(email, password);
      if (user) {
        const id = user.uid;
        await saveUserData(id, name)
          .then(res => {
            //   navigation.navigate('Home');
            console.log('signup sucess', res);
          })
          .catch(err => console.log('err save user', err));
      }
      setLoading(false);
    } catch (err: any) {
      setLoading(false);
      switch (err.code) {
        case 'auth/email-already-in-use':
          console.log('email already is use');
          break;
        case 'auth/weak-password':
          console.log('weak password. please choose a stronger password');
          break;
        default:
          console.log('signup error', err, err.code);
      }
    }
  };

  return (
    <View style={styles.container}>
      <Card>
        <Text style={styles.title}>Daftar Akun</Text>
        <View>
          <Input
            label={'Nama'}
            value={name}
            onChange={e => setName(e.nativeEvent.text)}
          />
          <ErrorMessage message={''} />
        </View>
        <View>
          <Input
            label={'Email'}
            value={email}
            onChange={e => setEmail(e.nativeEvent.text)}
          />
          <ErrorMessage message={''} />
        </View>
        <View>
          <Input
            label={'Password'}
            value={password}
            secureTextEntry
            onChange={e => setPassword(e.nativeEvent.text)}
          />
          <ErrorMessage message={''} />
        </View>
        <View>
          <Button onPress={handleSignup} isLoading={loading}>
            Daftar
          </Button>
        </View>
        <Text style={styles.loginText}>
          Sudah punya akun?{' '}
          <Text
            onPress={() => navigation.navigate('Login')}
            style={styles.loginTextChild}>
            Login Sekarang
          </Text>
        </Text>
      </Card>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 12,
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
  },
  loginText: {
    color: '#333',
    textAlign: 'center',
  },
  loginTextChild: {
    color: '#40A2E3',
  },
});
